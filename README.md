# Compact Website Styles

CSS styles for the [Stylus browser extension](https://addons.mozilla.org/en-GB/firefox/addon/styl-us/) that make supported websites use less padding and more compact.

## Installation

Install the [Stylus browser extension](https://addons.mozilla.org/en-GB/firefox/addon/styl-us/). Then simply open the raw `.user.css` file in the `css` folder corresponding to the site you wish to make more compact. Stylus will detect that you have opened a supported style and give the option to install it. Alternatively, click the "Install" links below to go directly to the raw files.

## Supported sites

- [Wallet by BudgetBakers](https://web.budgetbakers.com) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/wallet.user.css)
![Screenshot of some records in Wallet with the compact theme.](./screenshots/wallet.png)
- [ClickUp](https://app.clickup.com) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/clickup.user.css)
![Screenshot of a ClickUp task with the compact theme.](./screenshots/clickup.png)
- [GitLab](https://gitlab.com) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/gitlab.user.css)
![Screenshot of this GitLab repository's project overview with the compact theme.](./screenshots/gitlab.png)
- [Gmail](https://mail.google.com) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/gmail.user.css)
![Screenshot of a list of emails in Gmail with the compact theme.](./screenshots/gmail.png)
- [Google Contacts](https://contacts.google.com) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/google-contacts.user.css)

## Work in progress sites

- [StudySmarter](https://app.studysmarter.de) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/studysmarter.user.css)
- [Google Calendar](https://calendar.google.com) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/google-calendar.user.css)
- [Google Drive](https://drive.google.com) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/google-drive.user.css)
- [Google News](https://news.google.com) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/google-news.user.css)
- [Google My Activity](https://myactivity.google.com) - [Install](https://gitlab.com/siphomateke/compact-website-styles/-/raw/master/css/google-activity.user.css)
