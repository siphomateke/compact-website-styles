const gulp = require('gulp');
const sass = require('gulp-dart-sass');
const extReplace = require('gulp-ext-replace');

const src = './scss/**/*.scss';
const out = './css';

gulp.task('sass', () => gulp.src(src)
  .pipe(sass.sync().on('error', sass.logError))
  .pipe(extReplace('.user.css'))
  .pipe(gulp.dest(out))
);

gulp.task('sass:watch', () => {
  gulp.watch(src, gulp.series(['sass']));
});